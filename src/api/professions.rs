//! Struct definitions for the professions API endpoint.
//!
//! * [Example](https://api.guildwars2.com/v2/professions/Engineer)
//! * [Wiki](https://wiki.guildwars2.com/wiki/API:2/professions)

use super::HasId;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Profession {
    /// The profession id.
    pub id: String,
    /// The name of the profession.
    pub name: String,
    /// The numeric code of the profession, e.g. for chat links.
    pub code: u32,
    /// List of specialization ids.
    pub specializations: Vec<u32>,
    /// List of skills.
    pub skills: Vec<Skill>,
    /// Conversion of palette ID to skill ID.
    pub skills_by_palette: Vec<PaletteEntry>,
}

impl Profession {
    /// Resolves a given palette ID to the corresponding skill ID.
    pub fn palette_id_to_skill_id(&self, palette_id: u32) -> Option<u32> {
        self.skills_by_palette
            .iter()
            .find(|entry| entry.palette_id == palette_id)
            .map(|entry| entry.skill_id)
    }

    /// Resolves a given skill ID to the corresponding palette ID.
    pub fn skill_id_to_palette_id(&self, skill_id: u32) -> Option<u32> {
        self.skills_by_palette
            .iter()
            .find(|entry| entry.skill_id == skill_id)
            .map(|entry| entry.palette_id)
    }
}

impl HasId for Profession {
    type Id = String;
    fn get_id(&self) -> String {
        self.id.clone()
    }
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Skill {
    /// The id of the skill.
    pub id: u32,
    /// The skill bar slot that this skill can be used in.
    pub slot: String,
    #[serde(rename = "type")]
    pub typ: String,
}

#[derive(Debug, Clone)]
pub struct PaletteEntry {
    /// The palette ID, as used in the chat link.
    ///
    /// Note that the actual palette only allows 2 bytes for this number, i.e. an `u16`. To stay
    /// consistent with other integers that are handled here however, this struct uses a `u32`.
    pub palette_id: u32,
    /// The skill ID, as used in the API.
    pub skill_id: u32,
}

impl Serialize for PaletteEntry {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        (self.palette_id, self.skill_id).serialize(serializer)
    }
}

impl<'de> Deserialize<'de> for PaletteEntry {
    fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        let (palette_id, skill_id) = Deserialize::deserialize(deserializer)?;
        Ok(PaletteEntry {
            palette_id,
            skill_id,
        })
    }
}
