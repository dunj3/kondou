//! Struct definitions for the skills API endpoint.
//!
//! * [Example](https://api.guildwars2.com/v2/skills/14375)
//! * [Wiki](https://wiki.guildwars2.com/wiki/API:2/skills)

use super::HasId;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Skill {
    /// The skill id.
    pub id: u32,
    /// The skill name.
    pub name: String,
    /// A URL to an icon of the skill.
    pub icon: String,
}

impl HasId for Skill {
    type Id = u32;
    fn get_id(&self) -> u32 {
        self.id
    }
}
