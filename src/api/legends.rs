//! Struct definitions for the legends API endpoint.
//!
//! * [Example](https://api.guildwars2.com/v2/legends/Legend2)
//! * [Wiki](https://wiki.guildwars2.com/wiki/API:2/legends)

use super::HasId;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Legend {
    /// The legend id.
    pub id: String,
    pub swap: u32,
    pub heal: u32,
    pub elite: u32,
    pub utilities: Vec<u32>,
}

impl HasId for Legend {
    type Id = String;
    fn get_id(&self) -> String {
        self.id.clone()
    }
}
