//! Struct definitions for the traits API endpoint.
//!
//! * [Example](https://api.guildwars2.com/v2/traits/214)
//! * [Wiki](https://wiki.guildwars2.com/wiki/API:2/traits)

use super::HasId;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Trait {
    /// The trait id.
    pub id: u32,
    /// The trait name.
    pub name: String,
    /// The trait's icon URL.
    pub icon: String,
    /// The id of the specialization this trait belongs to.
    pub specialization: u32,
    /// The trait's tier, as a value from 1-3.
    ///
    /// Elite specializations also contain a tier 0 minor trait, describing which weapon the elite
    /// specialization gains access to.
    pub tier: u32,
    pub slot: Slot,
}

impl HasId for Trait {
    type Id = u32;
    fn get_id(&self) -> u32 {
        self.id
    }
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, Hash, Copy, Clone)]
pub enum Slot {
    Major,
    Minor,
}
