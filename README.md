kondou
======

`kondou` is a program that allows you to generate Guild Wars 2 build images (including traitlines and skills) with support for parsing and generating chat links as well, which allow you to link builds in-game.

Unlike the alternatives (see below), kondou is designed around non-interactive use, for example to batch-generate images to be included in offline guides or documents. kondou was written as [a result of](https://xkcd.com/1319/) screenshotting and embedding gw2skills images.

Features
========

* Finds skills and traitlines by name instead of ID.
* Can parse and generate chat codes.

Building
========

The complete build process is managed by `cargo`, the standard Rust package manager. As such, `cargo build --release` will download all required dependencies and build kondou in `target/release/kondou`.

Usage
=====

(See `kondou --help` for a list of all supported options)

Generate a build by specifying traits/skills by hand:

```
kondou Mesmer -s 'Signet of the Ether' -s 'Signet of Inspiration' -s 'Well of Action' -s 'Well of Senility' -s 'Time Warp' -t 'dueling:top:bot:top' -t 'inspiration:bot:mid:bot' -t 'chronomancer:mid:top:bot'
```

Generate a build by using a chat code:

```
kondou -c '[&DQcBHRc7KDYjDwAAgAEAAIcSAADPEgAAvAEAAAAAAAAAAAAAAAAAAAAAAAA=]'
```

Output:

```
Profession:    Mesmer
Skills:        Signet of the Ether
               Signet of Inspiration
               Well of Recall
               Well of Action
               Time Warp
Traitlines:    Dueling - top - bot - top
               Inspiration - bot - mid - bot
               Chronomancer - mid - top - bot

Chat code:     [&DQcBHRc7KDYjDwAAgAEAAIcSAADPEgAAvAEAAAAAAAAAAAAAAAAAAAAAAAA=]
Image saved in buildtemplate.png
```

Example image:

![Example output image](screenshots/buildtemplate.png)

You can change the output file location by using `-o filename` and you can suppress the output by using `-q`, in which case only the chat code will be printed (useful for usage in other scripts).

Alternatives
============

* [gw2skills](http://en.gw2skills.net) has more features and allows you to test out different stat combinations, runes, sigils, ... in your armor/weapons as well. However, it is designed for interactive use, which makes it harder to use in an automated fashion.
* [armory-embeds](https://github.com/madou/armory-embeds) are good for websites, but lack the feature to render a static image. Additionally, you have to use the skill/traitline IDs instead of the names, and you cannot use/generate chat codes.
