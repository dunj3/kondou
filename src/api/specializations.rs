//! Struct definitions for the specializations API endpoint.
//!
//! * [Example](https://api.guildwars2.com/v2/specializations/1)
//! * [Wiki](https://wiki.guildwars2.com/wiki/API:2/specializations)

use super::HasId;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Specialization {
    /// The specialization's ID.
    pub id: u32,
    /// The name of the specialization.
    pub name: String,
    /// The profession that this specialization belongs to.
    pub profession: String,
    /// `true` if this specialization is an elite specialization, `false` otherwise.
    pub elite: bool,
    /// A URL to an icon of the specialization.
    pub icon: String,
    /// An URL to the background image of the specialization.
    pub background: String,
    /// Contains a list of IDs specifying the minor traits in the specialization.
    pub minor_traits: Vec<u32>,
    /// Contains a list of IDs specifying the major traits in the specialization.
    pub major_traits: Vec<u32>,
}

impl HasId for Specialization {
    type Id = u32;
    fn get_id(&self) -> u32 {
        self.id
    }
}
